import sys
from collections import defaultdict
def max_quality(num_of_memes, x, y, win_chance, parent_nodes, nodes):
    sum_X = 0
    sum_Y = 0
    if len(parent_nodes) != 0 :
        for i in parent_nodes:
            maxx = []
            for j in nodes[i][1]:
                maxx.append(max(nodes[j][1]))
            tour = max(maxx)
            if tour in x :
                won_child = x.index(tour)
            else :
                won_child = y.index(tour)
            win_chance[won_child] = 1
    else:
        maxx = max(max(x), max(y))
        if maxx in x :
            won_child = x.index(maxx)
        else:
            won_child = y.index(maxx) 
        win_chance[won_child] = 1

    for i in range(len(x)):
        sum_X += win_chance[i] * x[i]

    for i in range(len(y)):
        sum_Y += win_chance[i] * y[i]

    return sum_X ** 2 + sum_Y ** 2

ipt = sys.stdin.readlines()
num_of_memes = int(ipt[0])
lst = list()
nodes = defaultdict(int)

for i in range(1, num_of_memes + 1):
    lst = list(map(int, ipt[i].split()))
    nodes[i] = (int(lst[0]), [int(j) for j in lst[1:]])

x = [0] * (num_of_memes + 1)
y = [0] * (num_of_memes + 1)
win_chance = [-1] * (num_of_memes + 1)

for i in nodes:
    if nodes[i][0] == 0 :
        x[i] = nodes[i][1][0]
        y[i] = nodes[i][1][1]

parent_nodes = list()

for i in nodes[1][1] :
    if nodes[i][0] != 0 :
        parent_nodes.append(i)

print(max_quality(num_of_memes, x, y, win_chance, parent_nodes, nodes))
