n = 4
nodes = {1: ( 3, [2,3,4]),
         2: ( 0,[10,1]),
         3: ( 0,[3,6]),
         4: ( 0 ,[2,7])}
w = [-1] * ( n + 1 )

def max_quality(n,x,y,parent_nodes):
    X = 0
    Y = 0
    if len(parent_nodes) != 0 :
        for i in parent_nodes:
            max1 = []
            for j in nodes[i][1]:
                max1.append(max(nodes[j][1]))
            t1 = max(max1)
            if t1 in x :
                k = x.index(t1)
            else :
                k = y.index(t1)
            w[k] = 1
    else:
        m = max(x)
        n = max(y)
        mn = max(m,n)
        if mn in x :
            i = x.index(mn)
        else:
            i = y.index(mn) 
        w[i] = 1
        
    for i in range(len(x)):
        X += w[i] * x[i]
    for i in range(len(y)):
        Y += w[i] * y[i]
    sum1 = X ** 2 + Y ** 2
    return sum1

x = [0] * ( n + 1 )
y = [0] * ( n + 1 )
for i in nodes:
    if nodes[i][0] == 0 :
        x[i] = nodes[i][1][0]
        y[i] = nodes[i][1][1]
    else:
        x[i] = 0
        y[i] = 0
parent_nodes = []
for i in nodes[1][1] :
    if nodes[i][0] != 0 :
        parent_nodes.append(i)
print(max_quality(n,x,y,parent_nodes))


